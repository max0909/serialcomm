// c++TestProjektSerialComm.cpp: Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include "SerialPort.h"
#include "LRMat.h"
#include <thread>
#include <iostream>

using namespace std;

int main()
{
#pragma region open comport
	SerialPort myPort = SerialPort();

	wchar_t aux[20];

	swprintf_s(aux, L"\\\\.\\COM5");

	myPort.open(aux);

	std::this_thread::sleep_for(std::chrono::milliseconds(200));

	cout << "Com Status: " << myPort.isOpened() << endl;
#pragma endregion

#pragma region Load LRMat
	LRMat myMat = LRMat();

	myMat.ReadFromFile("Matrix.lrmat");
#pragma endregion


#pragma region send_matrix

	//int overallCount = 22 * 11 * 7 * 4;
	//int count = 0;
	//unsigned int perc = 0;
	//for (int z1 = 0; z1 < 22; z1++)
	//{
	//	for (int z2 = 0; z2 < 11; z2++)
	//	{
	//		for (int z3 = 0; z3 < 7; z3++)
	//		{
	//			for (int z4 = 0; z4 < 4; z4++)
	//			{


	//				byte msg[255];
	//				unsigned int msgSize = 5;
	//				msg[0] = (byte)z1;// 0x00;
	//				msg[1] = (byte)z2;// 0x00;
	//				msg[2] = (byte)z3;// 0x00;
	//				msg[3] = (byte)z4;// 0x00;
	//				msg[4] = 127;// 0x7F;
	//				boolean result = false;
	//				do {
	//					result = myPort.writeBytes(msgSize, msg);
	//				} while (result == false);
	//				count++;
	//				unsigned int percNew = count * 100.0 / overallCount;
	//				if (percNew > perc)
	//				{
	//					perc = percNew;
	//					cout << perc << "%" << endl;
	//				}

	//			}
	//		}
	//	}
	//}

	myMat.TransferToArduino(myPort);

#pragma endregion

#pragma region recieve matrix
	/*byte msg[1];
	msg[0] = 0xFA;
	unsigned int msgSize = 1;
	boolean result = false;
	do {
		result = myPort.writeBytes(msgSize, msg);
	} while (result == false);

	while (true)
	{
		byte *results;
		results = myPort.readMatrix();
	}*/

	myMat.TransferFromArduino(myPort);


#pragma endregion

	myMat.SaveToFile("Matrix2.lrmat");

	

	system("Pause");
    return 0;
}

