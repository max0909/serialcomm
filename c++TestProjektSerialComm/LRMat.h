#pragma once
#include <string>
#include <windows.h>
#include "SerialPort.h"

using namespace std;

class LRMat
{
public:
	LRMat();
	~LRMat();

	void ReadFromFile(string path);

	void SaveToFile(string path);

	void ChangeValue(int x1, int x2, int x3, int x4, int value);

	void TransferToArduino(SerialPort port);

	void TransferFromArduino(SerialPort port);

	

private:

	const static int x1max = 22;
	const static int x2max = 11;
	const static int x3max = 7;
	const static int x4max = 4;

	string savePath;
	byte LR_Matrix[x1max][x2max][x3max][x4max];

};

