#include "stdafx.h"
#include "SerialPort.h"
#include <string>
#include <locale>
#include <codecvt>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>

inline std::string trim(std::string& str)
{
	str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
	str.erase(str.find_last_not_of(' ') + 1);         //surfixing spaces
	return str;
}



SerialPort::SerialPort()
{


}


SerialPort::~SerialPort()
{
}

//void SerialPort::open(string port)//
void SerialPort::open(wchar_t* port)
{
	DCB dcb;

	//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	//std::string narrow = converter.to_bytes(wide_utf16_source_string);
	//std::wstring wide = converter.from_bytes(port);

	this->serialPort = CreateFile(port, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	
	if (!GetCommState(serialPort, &dcb))
	{

		this->opened = false;
		return;
	}

	//Configure Serial Com-Port
	dcb.BaudRate = CBR_115200;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;
	dcb.fDtrControl = DTR_CONTROL_DISABLE;

	if (!SetCommState(serialPort, &dcb))
	{
		this->opened = false;
		return;
	}
	this->opened = true;
	return;

	
}

void SerialPort::close()
{
	if (CloseHandle(this->serialPort))
	{
		this->opened = false;
	}
}

boolean SerialPort::writeBytes(unsigned int payloadSize, byte * payload)
{

	const unsigned int PACKET_OVERHANG_BYTES = 3;
	const unsigned int PACKET_MAX_BYTES = 255;
	const byte PACKET_START_BYTE = 0xAA;

	//Check if serial port is opened, else break function
	if (!this->opened)
	{
		return false;
	}

#pragma region MyRegion
	////const int s = sizeof(bytes) + 1;
	////byte Bytes[s];
	////for (int i = 0; i < s-1; i++)
	////{
	////	Bytes[i] = bytes[i];
	////}
	////Bytes[s+1] = byte(10);

	//const int sizeArray = sizeof(bytes)+3;

	//byte buffer[sizeArray];

	//int i = 0;
	//while (i < (sizeArray - 2))
	//{
	//	buffer[i] = bytes[i];
	//	i++;
	//}
	//buffer[i++] = byte(13);
	//buffer[i] = byte(10);



	//const byte* writeBuffer = buffer;

	////for (int i = 0; i < sizeof(writeBuffer); i++)
	////{
	////	writeBuffer[i] = bytes[i];
	////}
	////writeBuffer[sizeof(writeBuffer)] = (byte)10;

	//////writeBuffer += 10;

	//DWORD rwWritten;

	////const byte* writeBuffer2 = writeBuffer;

	//int size = sizeArray;

	//WriteFile(this->serialPort, writeBuffer, size, &rwWritten, NULL);
#pragma endregion

	unsigned int packetSize = payloadSize + PACKET_OVERHANG_BYTES;

	static byte packet[PACKET_MAX_BYTES];

	packet[0] = PACKET_START_BYTE;
	packet[1] = packetSize;


	byte checkSum = packet[0] ^ packet[1];


	for (int i = 0; i < payloadSize; i++)
	{
		packet[i + 2] = payload[i];
		checkSum = checkSum ^ packet[i + 2];
	}


	packet[packetSize - 1] = checkSum;
	DWORD dwWritten;
	WriteFile(this->serialPort, packet, packetSize, &dwWritten, NULL);
	DWORD dwread = 0;
	unsigned int count = 0;
	do {
		this->readData(&dwread);
		count++;
		std::this_thread::sleep_for(std::chrono::milliseconds(2));
	} while (dwread == 0 && count <= 200);

	if (this->DataBuffer[0] == checkSum)
	{
		return true;
	}
	else
	{
		return false;
	}


}

boolean SerialPort::writeByte(byte b)
{
	DWORD dwWritten;
	return WriteFile(this->serialPort, &b, 1, &dwWritten, NULL);
}

void SerialPort::readData(DWORD *dwRead)
{
	if (this->opened)
	{
		//cout << "Port ist offen"<<endl; 
		//DWORD dwRead;
		DWORD temp;
		COMSTAT comstat;

		//Clear Buffer
		memset(this->DataBuffer, 0, sizeof(this->DataBuffer));

		//Clear Errors of Serial Communication
		ClearCommError(this->serialPort, &temp, &comstat);

		//cout << "cbInQue: " << comstat.cbInQue << endl;

		//New Bytes to read?
		if (comstat.cbInQue > 0)
		{
			ReadFile(this->serialPort, this->DataBuffer, comstat.cbInQue, dwRead, NULL);
			this->DataBuffer[*dwRead] = 0;
		}
	}
}

byte* SerialPort::readMatrix()
{
	DWORD temp;
	COMSTAT comstat;

	byte Buffer[1024];
	unsigned int BufferSize;

	int startindex = -1;

	//Clear Buffer
	memset(Buffer, 0, sizeof(Buffer));

	do {
		Buffer[0] = this->readByte();
	} while ((byte)Buffer[0] != 0xAA);

	do {
		Buffer[1] = this->readByte();
	} while (Buffer[1] == NULL);

	BufferSize = (unsigned int)Buffer[1];

	for (int i = 2; i < BufferSize; i++)
	{

		Buffer[i] = this->readByte();

	}

	byte checksum = 0x00;

	if (validate(BufferSize, Buffer, checksum))
	{
		for (int i = 0; i < BufferSize; i++)
		{
			cout << hex << (unsigned int)Buffer[i];
			cout << " ";
		}
		cout << endl;

		this->writeByte(checksum);

		byte returnVal[5];
		returnVal[0] = Buffer[2];
		returnVal[1] = Buffer[3];
		returnVal[2] = Buffer[4];
		returnVal[3] = Buffer[5];
		returnVal[4] = Buffer[6];

		return returnVal;
	}


}

byte SerialPort::readByte()
{
	DWORD temp;
	byte result = 0x00;
	DWORD bytesRead = 0;

	COMSTAT comstat;

	//Clear Errors of Serial Communication
	ClearCommError(this->serialPort, &temp, &comstat);
	//while (comstat.cbInQue <= 0)
	//{
	//	;
	//}
	//if (comstat.cbInQue > 0)
	//{
	//	ReadFile(this->serialPort, &result, 1, &bytesRead, NULL);
	//	return result;
	//}

	do {
		ReadFile(this->serialPort, &result, 1, &bytesRead, NULL);
	} while (bytesRead == 0);

	return result;
}

void SerialPort::printData()
{
	DWORD dwRead=0;
	do {
		this->readData(&dwRead);
	} while (dwRead == 0);
	for (int i = 0; i < dwRead; i++)
	{
		cout << hex << (unsigned int)this->DataBuffer[i];
		cout << " ";
	}
	cout << endl;

}

bool SerialPort::isOpened()
{
	//return this->opened;
	if (this->serialPort != INVALID_HANDLE_VALUE)
		return true;
	else
		return false;
}

bool SerialPort::validate(unsigned int msgSize, byte *msg, byte &check)
{
	if (msgSize < 3)
	{
		return false;
	}

	if (msgSize != (unsigned int)msg[1])
	{
		return false;
	}

	byte checksum = 0x00;
	for (int i = 0; i < msgSize - 1; i++)
	{
		checksum = checksum ^ msg[i];
	}

	if (checksum == msg[msgSize - 1])
	{
		check = checksum;
		return true;
	}

	return false;
}
