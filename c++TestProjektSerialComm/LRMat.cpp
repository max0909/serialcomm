#include "stdafx.h"
#include "LRMat.h"
#include "SerialPort.h"
#include <iostream>
#include <fstream>


using namespace std;


void SplitLineByComma(string s, string* output) 
{
	memset(output, 0, 5);
	string result[5];

	int z = 0;
	for (int i = 0; i < s.length(); i++)
	{
		if (s[i] == ',')
		{
			z++;
		}
		else
		{
			output[z] += s[i];
		}
	}

}


LRMat::LRMat()
{
	for (int x1 = 0; x1 < x1max; x1++)
	{
		for (int x2 = 0; x2 < x2max; x2++)
		{
			for (int x3 = 0; x3 < x3max; x3++)
			{
				for (int x4 = 0; x4 < x4max; x4++)
				{
					this->LR_Matrix[x1][x2][x3][x4] = 127;
				}
			}
		}
	}
}


LRMat::~LRMat()
{
}

void LRMat::ReadFromFile(string path)
{

	ifstream stream;

	stream.open(path);

	if (!stream.is_open())
	{
		return;
	}

	for (string line; getline(stream, line);)
	{
		string values[5];
		SplitLineByComma(line, values);
		
		int x1 = stoi(values[0]);
		int x2 = stoi(values[1]);
		int x3 = stoi(values[2]);
		int x4 = stoi(values[3]);
		int value = stoi(values[4]);

		this->ChangeValue(x1, x2, x3, x4, value);

	}

}

void LRMat::SaveToFile(string path)
{
	ofstream stream;

	stream.open(path);


	if (!stream.is_open())
	{
		return;
	}



	for (int x1 = 0; x1 < x1max; x1++)
	{
		for (int x2 = 0; x2 < x2max; x2++)
		{
			for (int x3 = 0; x3 < x3max; x3++)
			{
				for (int x4 = 0; x4 < x4max; x4++)
				{
					stream << x1 << "," << x2 << "," << x3 << "," << x4 << "," << (int)this->LR_Matrix[x1][x2][x3][x4] << endl;
				}
			}
		}
	}
	stream.close();
}

void LRMat::TransferToArduino(SerialPort port) 
{
	byte msg[1];
	msg[0] = 0xFB;
	unsigned int msgSize = 1;
	boolean result = false;
	do {
		result = port.writeBytes(msgSize, msg);
	} while (result == false);


	int maxVal = x1max * x2max*x3max*x4max;
	int act = 0;
	int percentage = -1;
	for (int x1 = 0; x1 < x1max; x1++)
	{
		for (int x2 = 0; x2 < x2max; x2++)
		{
			for (int x3 = 0; x3 < x3max; x3++)
			{
				for (int x4 = 0; x4 < x4max; x4++)
				{
					act++;
					/*byte sendingBytes[] = { (byte)x1,(byte)x2 ,(byte)x3,(byte)x4 ,  };

					port.writeBytesWithHS(sendingBytes);*/

					byte msg[255];
					unsigned int msgSize = 5;

					msg[0] = (byte)x1;// 0x00;
					msg[1] = (byte)x2;// 0x00;
					msg[2] = (byte)x3;// 0x00;
					msg[3] = (byte)x4;// 0x00;
					msg[4] = (byte)LR_Matrix[x1][x2][x3][x4];// 0x7F;

					boolean result = false;

					do {
						result = port.writeBytes(msgSize, msg);
					} while (result == false);

					int neu = (int)(act * 100 / maxVal);
					if (neu != percentage)
					{
						percentage = neu;
						cout << percentage << "%" << endl;
					}
					
				}
			}
		}
	}


	//Send End Signal
	byte msgEnd[255];
	unsigned int msgSizeEnd = 5;

	msgEnd[0] = 0xFF;// 0x00;
	msgEnd[1] = 0xFF;// 0x00;
	msgEnd[2] = 0xFF;// 0x00;
	msgEnd[3] = 0xFF;// 0x00;
	msgEnd[4] = 0xFF;// 0x00;

	boolean resultEnd = false;

	do {
		resultEnd = port.writeBytes(msgSizeEnd, msgEnd);
	} while (resultEnd == false);

	//byte sendingBytes[] = { 0,0 ,'E','N' ,'D' };
	//port.writeBytes(sendingBytes);
}

void LRMat::TransferFromArduino(SerialPort port)
{
	byte msg[1];
	msg[0] = 0xFA;
	unsigned int msgSize = 1;
	boolean result = false;
	do {
		result = port.writeBytes(msgSize, msg);
	} while (result == false);

	while (true)
	{
		byte *results;
		results = port.readMatrix();
		if (results[0] == 0xFF && results[1] == 0xFF && results[2] == 0xFF && results[3] == 0xFF && results[4] == 0xFF) //Springe raus wenn Endcode kommt
		{
			break;
		}


		this->LR_Matrix[(unsigned int)results[0]][(unsigned int)results[1]][(unsigned int)results[2]][(unsigned int)results[3]] = (unsigned int)results[4];
	}

	
		

}

void LRMat::ChangeValue(int x1, int x2, int x3, int x4, int value)
{

	if (x1 < x1max && x2 < x2max && x3 < x3max && x4 < x4max)
	{
		int val;
		if (value <= 255)
		{
			val = value;
		}
		else
		{
			val = 255;
		}
		this->LR_Matrix[x1][x2][x3][x4] = val;
	}

}
