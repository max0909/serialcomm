#pragma once
#include <string>
#include <windows.h>
using namespace std;

class SerialPort
{
public:
	SerialPort();
	~SerialPort();

	void open(wchar_t* port);

	void close();

	boolean writeBytes(unsigned int payloadSize, byte * payload);
	boolean writeByte(byte b);

	void readData(DWORD *dwRead);

	byte * readMatrix();

	byte readByte();

	void printData();

	bool isOpened();

	bool validate(unsigned int msgSize, byte *msg, byte &check);

private: 
	HANDLE serialPort;
	bool opened = false;


	//Databuffer
	byte DataBuffer[4096];
	byte OverHangBuffer[4096];

	string result;


};

